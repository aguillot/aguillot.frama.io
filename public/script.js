document.addEventListener("DOMContentLoaded", function() {
    // Select all tab buttons
    var tabLinks = document.getElementsByClassName("tab-btn");

    // addEventHandler to all buttons then add the correct event
    // Content to show is in data-tab field
    for (var i = 0; i < tabLinks.length; i++) {
        tabLinks[i].addEventListener("click", function(event) {
            openTab(event, this.getAttribute("data-tab"));
        });
    }

    // Simulate a click to load the first tab
    if (tabLinks.length > 0) {
        tabLinks[0].click();
    }
});

function openTab(event, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tab-content");
    // Setting all tab-content containers to display None
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Then we need to remove the tab-selected class
    tablinks = document.getElementsByClassName("tab-btn");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace("tab-selected", "");
    }

    // Finally, we need to active to selected part of the DOM and the correct Button
    document.getElementById(tabName).style.display = "block";
    event.currentTarget.className += " tab-selected";
}

